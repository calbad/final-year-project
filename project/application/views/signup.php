
    <?php
    $attributes = array('class' => 'form-signin');
    echo form_open('main/signup_validation', $attributes);
    if(isset($verify)){echo "<h3>$verify</h3>";}
    echo validation_errors();
    ?>
    <h2 class="form-signin-heading">Sign Up</h2>
    <label for="inputEmail">Email address</label>
    <input type="email" name="email" class="form-control" value="<?php echo $this->input->post('email'); ?>" placeholder="Enter Email">
    <label for="inputPassword">Password</label>
    <input type="password" name="password" class="form-control" placeholder="Enter Password">
    <label for="inputPassword">Confirm Password</label>
    <input type="password" name="cpassword" class="form-control" placeholder="Confirm Password">
    <button type="submit" name="signup_submit" class="btn btn-lg btn-primary btn-block">Sign Up</button>
    <a href='<?php echo base_url()."main/login";?>'>Back to Login</a>
    <?php
    echo form_close();
    ?>

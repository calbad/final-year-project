<div class="row">
  <div class="col-sm-12">
  <?php
  // this will show you thank you message.
  echo "<h1 align='center'>Thank You! Your order has been placed!</h1>";
  echo "<table class='table table-striped table-bordered table-condensed'>";
  echo "<tr>";
  echo "<td colspan='5'>";
  echo "Receipt Number: $order_id";
  echo "</td>";
  echo "</tr>";
  echo "<tr><td><strong>Name</strong></td><td><strong>Price</strong></td><td><strong>Qty</strong></td><td><strong>Subtotal</strong></td></tr>";
  foreach ($cart as $item):
    ?>
    <tr>
    <td>
      <?php echo $item['name']; ?>
    </td>
    <td>
      £<?php echo number_format($item['price'], 2); ?>
    </td>
    <td>
      <?php echo $item['qty']; ?>
    </td>
    <td>
      £<?php echo number_format($item['subtotal'], 2) ?>
    </td>
  </tr>
    <?php
    endforeach;
  //echo '<pre>'; print_r($test);
  echo "<tr>";
  echo "<td colspan='5'>";
  echo "Order Total: $total";
  echo "</td>";
  echo "</tr>";
  echo "</table>";
  ?>
</div>
</div>

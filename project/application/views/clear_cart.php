<script type="text/javascript">
  // To conform clear all data in cart.
  function clear_cart() {
    var result = confirm('Are you sure you want to clear your cart?');

    if (result) {
      window.location = "<?php echo base_url(); ?>cart/remove/all";
    } else {
      return false; // cancel button
    }
  }
  </script>

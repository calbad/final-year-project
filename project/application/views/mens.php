<div class="row">
  <div class="col-sm-12 main">
    <div id="cart" class="well">
      <div id="heading">
        <h2 align="center">Shopping Cart</h2>
      </div>

      <div id="text">
        <?php
        //http://www.formget.com/codeigniter-shopping-cart/ -- Used for its basic structure and then adpated to suit my needs
        $cart_check = $this->cart->contents();

        // If cart is empty, this will show below message.
        if(empty($cart_check)) {
          echo '<p align="center">To add products to cart click on "Add to Cart"</p>';
        }  ?>
      </div>

      <table id="table" border="0" cellpadding="5px" cellspacing="1px">
        <?php
        // All values of cart store in "$cart".
        if ($cart = $this->cart->contents()): ?>
        <tr id= "main_heading">
          <td>Name</td>
          <td>Price</td>
          <td>Qty</td>
          <td>Amount</td>
          <td>Cancel Product</td>
        </tr>
        <?php
        // Create form and send all values in "shopping/update_cart" function.
        echo form_open('cart/update_cart');
        echo validation_errors();
        $grand_total = 0;
        $i = 1;

        foreach ($cart as $item):
          //   echo form_hidden('cart[' . $item['id'] . '][id]', $item['id']);
          //  Will produce the following output.
          // <input type="hidden" name="cart[1][id]" value="1" />

          echo form_hidden('cart['.$item['id'].'][id]', $item['id']);
          echo form_hidden('cart['.$item['id'].'][rowid]', $item['rowid']);
          echo form_hidden('cart['.$item['id'].'][name]', $item['name']);
          echo form_hidden('cart['.$item['id'].'][price]', $item['price']);
          echo form_hidden('cart['.$item['id'].'][qty]', $item['qty']);
          ?>
          <tr>
            <td>
              <?php echo $item['name']; ?>
            </td>
            <td>
              £<?php echo number_format($item['price'], 2); ?>
            </td>
            <td>
              <?php echo form_input('cart['.$item['id'].'][qty]', $item['qty'], 'maxlength="1" size="1" style="text-align: right"'); ?>
            </td>
            <?php $grand_total = $grand_total + $item['subtotal']; ?>
            <td>
              £<?php echo number_format($item['subtotal'], 2) ?>
            </td>
            <td>
              <?php
              // cancel image.
              $burl = $this->config->base_url();
              $path = "<img src='".$burl."assets/img/cart_cross.jpg' width='25px' height='20px'>";
              echo anchor('cart/remove/' . $item['rowid'], $path); ?>
            </td>
          <?php endforeach; ?>
        </tr>
        <tr>
          <td colspan="6"  align="right">
            <b>Order Total: £<?php echo number_format($grand_total, 2); $test_total = number_format($grand_total, 2);?></b>
          </td>
        </tr>
        <tr>
          <td colspan="6" align="right">
            <?php echo form_hidden('test_total', $test_total); ?>
            <input type="button" class ='btn btn-primary btn-md' value="Clear" onclick="clear_cart()">
            <input type="submit" class ='btn btn-primary btn-md' name='update_order' value="Update">
            <input type="submit" class='btn btn-primary btn-md' name='place_order' value="Order">
            <?php echo form_close(); ?>

          </td>
        </tr>
      <?php endif; ?>
    </table>
  </div>
</div>
</div>
<div class="row">
  <div class="col-sm-12">
    <?php echo form_open('sort/index');
    //http://jasny.github.io/bootstrap/ -- Used to create the sidebar as it encompasses all the required code
    //This feature wasnt essential to the project, this is why I chose to use the third party software
    ?>
    <div class="navmenu navmenu-default navmenu-fixed-left offcanvas">
      <a class="navmenu-brand" href="#">Sort The Items</a>
      <ul class="nav navmenu-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Colour<b class="caret"></b></a>
          <ul class="dropdown-menu navmenu-nav">
            <li><button type="submit" class="sb-link" name="sort-red" value="red">Red</button></li>
            <li><button type="submit" class="sb-link" name="sort-blue" value="blue">Blue</button></li>
            <li><button type="submit" class="sb-link" name="sort-grey" value="grey">Grey</button></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Size<b class="caret"></b></a>
          <ul class="dropdown-menu navmenu-nav">
            <li class="dropdown-header">Shirts</li>
            <li><button type="submit" class="sb-link" name="sort-small" value="small">Small</button></li>
            <li><button type="submit" class="sb-link" name="sort-medium" value="medium">Medium</button></li>
            <li><button type="submit" class="sb-link" name="sort-large" value="large">Large</button></li>
            <li class="divider"></li>
            <li class="dropdown-header">Trousers</li>
            <li><button type="submit" class="sb-link" name="sort-32R" value="32R">32R</button></li>
            <li><button type="submit" class="sb-link" name="sort-32L" value="32L">32L</button></li>
            <li><button type="submit" class="sb-link" name="sort-34R" value="34R">34R</button></li>
            <li><button type="submit" class="sb-link" name="sort-34L" value="34L">34L</button></li>
            <li class="divider"></li>
            <li class="dropdown-header">Shoes</li>
            <li><button type="submit" class="sb-link" name="sort-9" value="9">9</button></li>
            <li><button type="submit" class="sb-link" name="sort-10" value="10">10</button></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product<b class="caret"></b></a>
          <ul class="dropdown-menu navmenu-nav">
            <li><button type="submit" class="sb-link" name="sort-shirts" value="shirts">Shirts</button></li>
            <li><button type="submit" class="sb-link" name="sort-trousers" value="trousers">Trousers</button></li>
            <li><button type="submit" class="sb-link" name="sort-shoes" value="shoes">Shoes</button></li>
          </ul>
        </li>
        <li><button type="submit" class="sb-link" name="sort-all" value="all">Show All</button></li>
      </ul>
    </div>
    <?php echo form_close(); ?>
    <div class="navbar navbar-default navbar-b">
      <button type="button" class="btn btn-primary" data-toggle="offcanvas" data-target=".navmenu" data-canvas="body">
        Sort Items
      </button>
    </div>
  </div>
</div>
<?php
$count = 0;
// "$products" sent from "shopping" controller,its stores all product which available in database.
foreach ($products as $product) {
  $id = $product['serial'];
  $name = $product['name'];
  $description = $product['description'];
  $price = $product['price'];

  if($count==0 OR is_int($count/3)){
    echo '<div class="row">';
  }
  ?>
  <div class="col-sm-4">
    <div id='product_div'>
      <div id='image_div'>
        <img src="<?php echo base_url() . $product['picture'] ?>" class="img-responsive"/>
      </div>
      <div id='info_product'>
        <div id='name'><?php echo $name; ?>
        </div>
        <div id='desc'>  <?php echo $description; ?>
        </div>
        <div id='rs'><b>Price</b>:<big style="color:#337ab7">
          £<?php echo $price; ?></big>
        </div>
        <?php

        // Create form and send names in 'cart/add' function.
        echo form_open('cart/add');
        echo form_hidden('id', $id);
        echo form_hidden('name', $name);
        echo form_hidden('price', $price);
        ?>
      </div>
      <div id='add_button'>
        <?php
        $btn = array(
          'class' => 'btn btn-primary btn-md',
          'value' => 'Add to Cart',
          'name' => 'action'
        );

        // Submit Button.
        echo form_submit($btn);
        echo form_close();
        ?>
      </div>
    </div>
  </div>
  <?php if($count==2 || $count==5){
    echo '</div>';
  }
  $count++;
}
?>

<div class="row">
  <div class="col-sm-12">
    <h1>Members Page</h1>
  </div>
</div>
<div class="row">
  <div class="col-sm-4">
    <?php
    if (isset($addYes))
    {
      ?>
      <div id="form-signin">
        <?php
        $attributes = array('class' => 'form-signin');
        echo form_open('main/address_validation#form-signin', $attributes);
        ?>
        <h2 class="form-signin-heading">Address Details</h2>
        <?php if (isset($AupdateYes)){echo "<h3 class='text-center'>$updated</h3>";}?>
        <label for="inputHouse">House No./Name</label>
        <?php echo form_error('house'); ?>
        <input type="text" name="house" class="form-control" value="<?php echo set_value('house', $address['house']); ?>" placeholder="Enter House No./Name">
        <label for="inputStreet">Street</label>
        <?php echo form_error('street'); ?>
        <input type="text" name="street" class="form-control" value="<?php echo set_value('street', $address['street']); ?>" placeholder="Enter Street Name">
        <label for="inputTown">Town</label>
        <?php echo form_error('town'); ?>
        <input type="text" name="town" class="form-control" value="<?php echo set_value('town', $address['town']); ?>" placeholder="Enter Town Name">
        <label for="inputCity">City</label>
        <?php echo form_error('city'); ?>
        <input type="text" name="city" class="form-control" value="<?php echo set_value('city', $address['city']); ?>" placeholder="Enter City Name">
        <label for="inputCounty">County</label>
        <?php echo form_error('county'); ?>
        <input type="text" name="county" class="form-control" value="<?php echo set_value('county', $address['county']); ?>" placeholder="Enter County Name">
        <label for="inputPostcode">Postcode</label>
        <?php echo form_error('postcode'); ?>
        <input type="text" name="postcode" class="form-control" value="<?php echo set_value('postcode', $address['postcode']); ?>" placeholder="Enter Postcode">
        <button type="submit" name="address_submit" value="submit" class="btn btn-lg btn-primary btn-block">Update Address</button>
        <?php
        echo form_close();
        ?>
      </div>

      <?php
    }
    else
    {
      $attributes = array('class' => 'form-signin', 'id' => 'form-address');
      echo form_open('main/address_validation', $attributes);
      if(isset($noAdd)){echo "<h2 class='error'>$noAdd</h2>";}
      ?>
      <h2 class="form-signin-heading">Add Address</h2>
      <label for="inputHouse">House No./Name</label>
      <?php echo form_error('house'); ?>
      <input type="text" name="house" class="form-control" value="<?php echo $this->input->post('email'); ?>" placeholder="Enter House No./Name">
      <label for="inputStreet">Street</label>
      <?php echo form_error('street'); ?>
      <input type="text" name="street" class="form-control" placeholder="Enter Street Name">
      <label for="inputTown">Town</label>
      <?php echo form_error('town'); ?>
      <input type="text" name="town" class="form-control" placeholder="Enter Town Name">
      <label for="inputCity">City</label>
      <?php echo form_error('city'); ?>
      <input type="text" name="city" class="form-control" placeholder="Enter City Name">
      <label for="inputCounty">County</label>
      <?php echo form_error('county'); ?>
      <input type="text" name="county" class="form-control" placeholder="Enter County Name">
      <label for="inputPostcode">Postcode</label>
      <?php echo form_error('postcode'); ?>
      <input type="text" name="postcode" class="form-control" placeholder="Enter Postcode">
      <button type="submit" name="new_submit" value="submit" class="btn btn-lg btn-primary btn-block">Add Address</button>
      <?php
      echo form_close();
    }
    ?>
  </div>
  <div class="col-sm-4">
    <div id="login-update">
      <?php
      $attributes = array('class' => 'form-signin');
      echo form_open('main/uLogin_validation#login-update', $attributes);
      ?>
      <h2 class="form-signin-heading">Login Details</h2>
      <?php if (isset($LupdateYes)){echo "<h3 class='text-center'>$updated</h3>";}?>
      <label for="inputEmail">Email</label>
      <?php echo form_error('email'); ?>
      <input type="text" name="email" class="form-control" value="<?php echo set_value('email', $email); ?>" placeholder="Enter New Email Address">
      <label for="inputOpass">Old Password</label>
      <?php echo form_error('opass'); ?>
      <input type="password" name="opass" class="form-control" placeholder="Enter Old Password">
      <label for="inputNpass">New Password</label>
      <?php echo form_error('npass'); ?>
      <input type="password" name="npass" class="form-control" placeholder="Enter New Password">
      <label for="inputCpass">Confirm New Password</label>
      <?php echo form_error('cpass'); ?>
      <input type="password" name="cpass" class="form-control" placeholder="Confirm New Password">
      <button type="submit" name="address_submit" class="btn btn-lg btn-primary btn-block">Update Login Details</button>
      <?php
      echo form_close();
      ?>
    </div>
  </div>
  <div class="col-sm-4">
    <div class="order-history">
      <h2 class="form-signin-heading">Order History</h2>
      <div>
        <?php
        if(is_array($orders) && count($orders) ) {
          ?>
        <table class="table table-striped table-bordered table-condensed">
          <tr><td><strong>Date</strong></td><td><strong>Product Name</strong></td><td><strong>Qty</strong></td><td><strong>Price</strong></td><td><strong>Subtotal</strong></td></tr>
            <?php
            foreach($orders as $order){
              ?>
              <tr><td><?=date("j M Y", strtotime($order['date']));?></td><td><?=$order['product_name'];?></td><td><?=$order['quantity'];?></td><td><?=$order['price'];?></td><td><?=$order['subtotal'];?></td></tr>
              <?php
            }
          }
          else {
            echo "<h3>You haven't placed any orders yet</h3>";
          }

          ?>
        </table>
      </div>
      <div>
        <?php echo $this->pagination->create_links(); //http://stackoverflow.com/questions/12164114/codeigniter-bootstrap-pagination -- Used to aid with pagination ?>
      </div>
    </div>
  </div>
</div>

		<?php
		$attributes = array('class' => 'form-signin');
		echo form_open('main/login_validation', $attributes);
		echo validation_errors();
		?>
		<h2 class="form-signin-heading">Login</h2>
		<label for="inputEmail" class="sr-only">Email address</label>
		<input type="email" name="email" class="form-control" value="<?php echo $this->input->post('email'); ?>" placeholder="Enter Email">
		<label for="inputPassword" class="sr-only">Password</label>
		<input type="password" name="password" class="form-control" placeholder="Enter Password">
		<button type="submit" name="login_submit" class="btn btn-lg btn-primary btn-block">Login</button>
		<a href='<?php echo base_url()."main/signup";?>'>Sign Up</a>
		<?php
		echo form_close();
		?>

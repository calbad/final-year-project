<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title ?></title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="<?php echo base_url("assets/css/bootstrap.css"); ?>" />
	<link rel="stylesheet" href="<?php echo base_url("assets/css/jasny-bootstrap.min.css"); ?>" />
	<link rel="stylesheet" href="<?php echo base_url("assets/css/dataTables.bootstrap.css"); ?>" />
	<link rel="stylesheet" href="<?php echo base_url("assets/css/main.css"); ?>" />
	<link rel="shortcut icon" href="<?php echo base_url("assets/img/favicon.ico"); ?>">
	<?php if (isset($script)) { $this->load->view('clear_cart'); } ?>
</head>
<body>
<script>
// Hide content onload, prevents JS flicker
document.body.className += ' js-enabled';
</script>
<div class="container" id="container">
	<div class="row top-buffer">
		<div class="col-md-12 header">
			<a href='<?php echo base_url()."main/home";?>'><img src="<?php echo base_url("assets/img/portheader.png"); ?>"  class"img-responsive" alt="Header"></a>
		</div>
	</div>
	<!-- Static navbar -->
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="nav navbar-nav">
					<li><a href='<?php echo base_url()."main/home";?>'>Home</a></li>
					<li><a href='<?php echo base_url()."cart/index";?>'>Mens</a></li>
					<!--<li><a href='<?php //echo base_url()."main/womens";?>'>Womens</a></li>-->
					<?php
					$burl = $this->config->base_url();
					if($this->session->userdata('is_logged_in'))
					{
						echo "<li><a href='".$burl."main/members'>Members</a></li>";
						echo "<li><a href='".$burl."main/logout'>Logout</a></li>";
					}
					if(!$this->session->userdata('is_logged_in'))
					{
						echo "<li><a href='".$burl."main/login'>Login</a></li>";
						echo "<li><a href='".$burl."main/signup'>Sign Up</a></li>";
					}
					?>
					<!--
					<li><a href='<?php //echo base_url()."main/login";?>'>Login</a></li>
					<li><a href='<?php //echo base_url()."main/signup";?>'>Sign Up</a></li>
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">News <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
					<li><a href='<?php //echo base_url()."news/index";?>'>View</a></li>
					<li><a href='<?php //echo base_url()."news/create";?>'>Create</a></li>
				</ul>
			</li>-->
		</ul>
		<?php
		if($this->session->userdata('is_logged_in'))
		{
			?>
			<p class="nav navbar-text navRight">Logged in as: <?php echo $this->session->userdata('email'); ?></p>
			<?php } ?>
		</div><!--/.nav-collapse -->
	</div><!--/.container-fluid -->
</nav>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$this->home();
	}

	public function home()
	{
		$data['title'] = 'Homepage';

		$this->load->view('templates/header', $data);
		$this->load->view('home');
		$this->load->view('templates/footer');
	}

	public function mens()
	{
		$data['title'] = 'Mens';

		$this->load->view('templates/header', $data);
		$this->load->view('mens');
		$this->load->view('templates/footer');
	}

	public function sidebar()
	{
		$data['title'] = 'Sidebar';

		$this->load->view('templates/header', $data);
		$this->load->view('sidebar');
		$this->load->view('templates/footer');
	}

	public function womens()
	{
		$data['title'] = 'Womens';

		$this->load->view('templates/header', $data);
		$this->load->view('womens');
		$this->load->view('templates/footer');
	}

	public function login()
	{
		$data['title'] = 'Login';

		$this->load->view('templates/header', $data);
		$this->load->view('login');
		$this->load->view('templates/footer');
	}

	public function signup()
	{
		$data['title'] = 'Sign Up';

		$this->load->view('templates/header', $data);
		$this->load->view('signup');
		$this->load->view('templates/footer');
	}

	public function members($offset = null)
	{
		$this->load->model('cart_model');
		$this->load->library('pagination');
		//http://stackoverflow.com/questions/12164114/codeigniter-bootstrap-pagination -- Used to aid with pagination
		$config['base_url'] = base_url(). 'main/members';
		$config['total_rows'] = $this->cart_model->allOrders();
		$config['per_page'] = 4;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
		$this->pagination->initialize($config);
		$this->pagination->cur_page = $offset;
		$query = $this->cart_model->getOrders($config['per_page'],$offset);

		$orderData = null;

		if($query){
			$orderData =  $query;
		}
		if($this->session->userdata('is_logged_in'))
		{
			$this->load->model('model_users');
			$user_id = $this->session->userdata('user_id');
			if($this->model_users->check_address($user_id))
			{
				$data = array(
					'title' => "TrueMembers",
					'addYes' => TRUE,
					'address' => $this->model_users->get_address($user_id),
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			else
			{
				$data = array(
					'title' => "FalseMembers",
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			$this->output->set_header("HTTP/1.0 200 OK");
			$this->output->set_header("HTTP/1.1 200 OK");
			$this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
			$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
			$this->output->set_header("Cache-Control: post-check=0, pre-check=0");
			$this->output->set_header("Pragma: no-cache");
			$this->load->view('templates/header', $data);
			$this->load->view('members', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			redirect('main/restricted');
		}
	}

	public function restricted()
	{
		$data['title'] = 'Restricted';

		$this->load->view('templates/header', $data);
		$this->load->view('restricted');
		$this->load->view('templates/footer');
	}

	public function login_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|htmlspecialchars|xss_clean|max_length[40]|callback_validate_credentials');
		$this->form_validation->set_rules('password', 'Password', 'required|htmlspecialchars|min_length[6]|max_length[20]|trim');
		$this->load->model('model_users');
		if($this->form_validation->run())
		{
			$user_id = $this->model_users->get_userID($this->input->post('email'));
			$data = array(
				'email' => $this->input->post('email'),
				'is_logged_in' => 1,
				'user_id' => $user_id
			);
			$this->session->set_userdata($data);
			redirect('main/members');
		}
		else
		{
			$data['title'] = 'Login';
			$this->load->view('templates/header', $data);
			$this->load->view('login');
			$this->load->view('templates/footer');
		}
	}

	public function signup_validation()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email',
		'required|trim|htmlspecialchars|valid_email|max_length[40]|is_unique[user.email]');

		$this->form_validation->set_rules('password', 'Password',
		'required|htmlspecialchars|min_length[6]|max_length[20]|trim');

		$this->form_validation->set_rules('cpassword', 'Confirm Password',
		'required|htmlspecialchars|trim|min_length[6]|max_length[20]|matches[password]');

		$this->form_validation->set_message('is_unique', "That email address is already in use.");

		if ($this->form_validation->run())
		{
			//generate a random key
			$key = md5(uniqid());

			//send email to user
			$this->load->library('email', array('mailtype'=>'html'));
			$this->load->model('model_users');
			$this->email->from('admin@178.62.89.219', "Callum");
			$this->email->to($this->input->post('email'));
			$this->email->subject("Confirm your account");

			$message = "<p>Thank you for signing up.</p>";
			$message .= "<p><a href='".base_url()."main/register_user/$key'>Click here</a> to confirm your account</p>";

			$this->email->message($message);

			if($this->model_users->add_temp_user($key))
			{
				if($this->email->send())
				{
					$data = array(
						'title' => 'Sign Up',
						'verify' => 'Thank you for registering. We have emailed a link so you can verify your account.'
					);
				}
				else
				{
					$data = array(
						'title' => 'Sign Up',
						'verify' => 'Unable to send email. Please try again later.'
					);
				}

				$this->load->view('templates/header', $data);
				$this->load->view('signup', $data);
				$this->load->view('templates/footer');
			}
			else
			{
				echo "Problem adding to database.";
			}
			//add user to temp database db

		}
		else
		{
			$data['title'] = 'Sign Up';

			$this->load->view('templates/header', $data);
			$this->load->view('signup');
			$this->load->view('templates/footer');
		}

	}

	public function address_validation($offset = null)
	{
		$this->load->model('cart_model');
		$this->load->model('model_users');
		$this->load->library('pagination');

		$config['base_url'] = base_url(). 'main/members';
		$config['total_rows'] = $this->cart_model->allOrders();
		$config['per_page'] = 4;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
		$this->pagination->initialize($config);
		$this->pagination->cur_page = $offset;
		$query = $this->cart_model->getOrders($config['per_page'],$offset);

		$orderData = null;

		if($query){
			$orderData =  $query;
		}
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		$this->form_validation->set_rules('house', 'House No./Name', 'required|htmlspecialchars|trim|max_length[20]|xss_clean');
		$this->form_validation->set_rules('street', 'Street', 'required|htmlspecialchars|trim|max_length[25]|xss_clean');
		$this->form_validation->set_rules('town', 'Town', 'required|htmlspecialchars|trim|max_length[20]|xss_clean');
		$this->form_validation->set_rules('city', 'City', 'required|htmlspecialchars|trim|max_length[20]|xss_clean');
		$this->form_validation->set_rules('county', 'Country', 'required|htmlspecialchars|trim|max_length[20]|xss_clean');
		$this->form_validation->set_rules('postcode', 'Postcode', 'required|htmlspecialchars|max_length[8]|trim|xss_clean');

		if ($this->form_validation->run())
		{
			$user_id = $this->session->userdata('user_id');
			if($this->input->post('address_submit') == 'submit')
			{
				$data = array(
					'house' => $this->input->post('house'),
					'street' => $this->input->post('street'),
					'town' => $this->input->post('town'),
					'city' => $this->input->post('city'),
					'county' => $this->input->post('county'),
					'postcode' => $this->input->post('postcode')
				);
				$id_array = $this->model_users->get_address($user_id);
				$id = $id_array['id'];

				$this->db->where('id', $id);
				$this->db->update('address', $data);
			}
			if($this->input->post('new_submit') == 'submit')
			{
				$address = array(
					'house' => $this->input->post('house'),
					'street' => $this->input->post('street'),
					'town' => $this->input->post('town'),
					'city' => $this->input->post('city'),
					'county' => $this->input->post('county'),
					'postcode' => $this->input->post('postcode'),
					'user_id' => $this->session->userdata('user_id')
				);

				$this->model_users->insert_address($address);
			}

			if($this->model_users->check_address($user_id))
			{
				$data = array(
					'title' => "Members",
					'addYes' => TRUE,
					'AupdateYes' => TRUE,
					'address' => $this->model_users->get_address($user_id),
					'updated' => "Address Updated",
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			else
			{
				$data = array(
					'title' => "WrongMembers",
					'orders' => $orderData
				);
			}
			$this->load->view('templates/header', $data);
			$this->load->view('members', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->load->model('model_users');
			$user_id = $this->session->userdata('user_id');
			if($this->model_users->check_address($user_id))
			{
				$data = array(
					'title' => "TrueMembers",
					'addYes' => TRUE,
					'address' => $this->model_users->get_address($user_id),
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			else
			{
				$data = array(
					'title' => "FalseMembers",
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			$this->load->view('templates/header', $data);
			$this->load->view('members', $data);
			$this->load->view('templates/footer');
		}

	}

	public function uLogin_validation($offset = null)
	{
		$this->load->model('cart_model');
		$this->load->model('model_users');
		$this->load->library('pagination');

		$config['base_url'] = base_url(). 'main/members';
		$config['total_rows'] = $this->cart_model->allOrders();
		$config['per_page'] = 4;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
		$this->pagination->initialize($config);
		$this->pagination->cur_page = $offset;
		$query = $this->cart_model->getOrders($config['per_page'],$offset);

		$orderData = null;

		if($query){
			$orderData =  $query;
		}

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<p class="error">', '</p>');
		$required_if = $this->input->post('npass') ? '|required' : '' ;
		$this->form_validation->set_rules('email', 'Email',
		'required|htmlspecialchars|trim|valid_email|max_length[40]|callback_validate_email');
		$this->form_validation->set_rules('opass', 'Current Password', 'required|min_length[6]|max_length[20]|htmlspecialchars|trim|callback_validate_pass');
		$this->form_validation->set_rules('npass', 'New Password', 'min_length[6]|max_length[20]|htmlspecialchars|trim');
		$this->form_validation->set_rules('cpass', 'Confirm New Password',
		'htmlspecialchars'. $required_if .'|max_length[20]|trim|matches[npass]');

		if(!empty($this->input->post('npass')) && empty($this->input->post('cpass')))
		{
			$this->form_validation->set_message('required', "Confirm Password is required when changing password.");
		}

		if ($this->form_validation->run())
		{
			if(!empty($this->input->post('npass')))
			{
				$data = array(
					'email' => $this->input->post('email'),
					'password' => password_hash($this->input->post('npass'), PASSWORD_DEFAULT)
				);
			}
			else if (empty($this->input->post('npass')))
			{
				$data = array(
					'email' => $this->input->post('email'),
					'password' => password_hash($this->input->post('opass'), PASSWORD_DEFAULT)
				);
			}

			$this->load->model('model_users');
			$user_id = $this->session->userdata('user_id');
			$this->db->where('id', $user_id);
			$this->db->update('user', $data);
			$this->session->set_userdata('email', $this->input->post('email'));
			if($this->model_users->check_address($user_id))
			{
				$data = array(
					'title' => "Members",
					'addYes' => TRUE,
					'LupdateYes' => TRUE,
					'address' => $this->model_users->get_address($user_id),
					'updated' => "Login Details Updated",
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			else
			{
				$data = array(
					'title' => "Members",
					'LupdateYes' => TRUE,
					'updated' => "Login Details Updated",
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			$this->load->view('templates/header', $data);
			$this->load->view('members', $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->load->model('model_users');
			$user_id = $this->session->userdata('user_id');
			if($this->model_users->check_address($user_id))
			{
				$data = array(
					'title' => "TrueMembers",
					'addYes' => TRUE,
					'address' => $this->model_users->get_address($user_id),
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}
			else
			{
				$data = array(
					'title' => "FalseMembers",
					'email' => $this->session->userdata('email'),
					'orders' => $orderData
				);
			}

			$this->load->view('templates/header', $data);
			$this->load->view('members', $data);
			$this->load->view('templates/footer');
		}

	}

	public function validate_credentials()
	{
		$this->load->model('model_users');
		if($this->model_users->can_log_in())
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_credentials','Incorrect email/password.');
			return false;
		}
	}
	public function validate_pass()
	{
		$this->load->model('model_users');
		$user_id = $this->session->userdata('user_id');
		if($this->model_users->check_pass($user_id))
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message('validate_pass','This isn\'t your current password.');
			return false;
		}
	}

	public function validate_email()
	{
		$this->load->model('model_users');
		$user_id = $this->session->userdata('user_id');
		if($this->model_users->check_email($user_id, $this->input->post('email')))
		{
			$this->form_validation->set_message('validate_email','Email already in use.');
			return false;
		}
		else
		{

			return true;
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('main/login');
	}

	public function register_user($key)
	{
		//https://www.youtube.com/watch?v=FmKm1gCgUoM -- Tutorial followed to create register/login, but then adapted to suit my needs
		$this->load->model('model_users');
		if($this->model_users->is_key_valid($key))
		{
			if($newemail = $this->model_users->add_user($key))
			{

				$data = array(
					'email' => $newemail,
					'is_logged_in' => 1
				);
				$this->session->set_userdata($data);
				redirect('main/members');
			}
			else
			{
				echo "Failed to add user. Please try again.";
			}
		}
		else
		{
			echo "Invalid key";
		}

	}

}

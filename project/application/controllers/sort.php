<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sort extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    //load model
    $this->load->model('cart_model');
    $this->load->library('cart');
  }

  public function index()
  {
    if($this->input->post('sort-red'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_colour($this->input->post('sort-red'))
      );
    }
    if($this->input->post('sort-blue'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_colour($this->input->post('sort-blue'))
      );
    }
    if($this->input->post('sort-grey'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_colour($this->input->post('sort-grey'))
      );
    }

    if($this->input->post('sort-small'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-small'))
      );
    }

    if($this->input->post('sort-medium'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-medium'))
      );
    }

    if($this->input->post('sort-large'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-large'))
      );
    }

    if($this->input->post('sort-9'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-9'))
      );
    }
    if($this->input->post('sort-10'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-10'))
      );
    }
    if($this->input->post('sort-32R'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-32R'))
      );
    }
    if($this->input->post('sort-32L'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-32L'))
      );
    }
    if($this->input->post('sort-34R'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-34R'))
      );
    }
    if($this->input->post('sort-34L'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_size($this->input->post('sort-34L'))
      );
    }
    if($this->input->post('sort-shirts'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_type($this->input->post('sort-shirts'))
      );
    }
    if($this->input->post('sort-trousers'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_type($this->input->post('sort-trousers'))
      );
    }
    if($this->input->post('sort-shoes'))
    {
      $data = array(
        'title' => "Mens",
        'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
        'products' => $this->cart_model->get_type($this->input->post('sort-shoes'))
      );
    }
    if($data == NULL)
    {
      redirect('cart/index');
    }

    $this->load->view('templates/header', $data);
    $this->load->view('mens', $data);
    $this->load->view('templates/footer');
  }

}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//http://omnipay.thephpleague.com/ --Omnipay library used to help intergrate PayPal
use Omnipay\Common\GatewayFactory;
class Cart extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    //load model
    $this->load->model('cart_model');
    $this->load->library('cart');
  }

  public function index()
  {
    $data = array(
      'title' => "Mens",
      'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
      'products' => $this->cart_model->get_all()
    );

    $this->load->view('templates/header', $data);
    $this->load->view('mens', $data);
    $this->load->view('templates/footer');
  }


  function add()
  {
    // Set array for send data.
    $insert_data = array(
      'id' => $this->input->post('id'),
      'name' => $this->input->post('name'),
      'price' => $this->input->post('price'),
      'qty' => 1
    );

    // This function adds items into cart.
    $this->cart->insert($insert_data);

    // This will show inserted data in cart.
    redirect('cart');
  }

  function remove($rowid) {
    // Check rowid value.
    if ($rowid==="all"){
      // Destroy data which is store in session.
      $this->cart->destroy();
    }else{
      // Destroy selected rowid in session.
      $data = array(
        'rowid'   => $rowid,
        'qty'     => 0
      );
      // Update cart data, after cancel.
      $this->cart->update($data);
    }

    // This will show cancel data in cart.
    redirect('cart');
  }

  function update_cart(){
    if ($cart = $this->cart->contents()):
      foreach ($cart as $item):
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
        $this->form_validation->set_rules('cart['.$item['id'].'][qty]', 'Quantity', 'required|htmlspecialchars|trim|max_length[1]|xss_clean|numeric');
        $this->form_validation->set_message('max_length', "You cannot purchase more than 9 of a single item.");
        if ($this->form_validation->run())
        {
          if($this->input->post('update_order'))
          {// Recieve post values,calculate them and update
            $cart_info =  $_POST['cart'] ;
            foreach( $cart_info as $id => $cart)
            {
              $rowid = $cart['rowid'];
              $price = $cart['price'];
              $amount = $price * $cart['qty'];
              $qty = $cart['qty'];

              $data = array(
                'rowid'   => $rowid,
                'price'   => $price,
                'amount' =>  $amount,
                'qty'     => $qty
              );

              $this->cart->update($data);
            }
            redirect('cart');
          }
        }
        else{
          $data = array(
            'title' => "Mens",
            'script' => TRUE, // if you omit or comment this variable the 'views/script.php' is not loaded
            'products' => $this->cart_model->get_all()
          );

          $this->load->view('templates/header', $data);
          $this->load->view('mens', $data);
          $this->load->view('templates/footer');
        }
      endforeach;
    endif;

    if($this->input->post('place_order'))
    {
      if($this->session->userdata('is_logged_in'))
      {
        $this->load->model('model_users');
        $user_id = $this->session->userdata('user_id');
        if($this->model_users->check_address($user_id))
        {
          $this->session->set_userdata('total', $this->input->post('test_total'));
          $gateway = GatewayFactory::create('PayPal_Express');
          $gateway->setUsername('calbad-facilitator_api1.hotmail.co.uk');
          $gateway->setPassword('J68RB9TEW9KG79F2');
          $gateway->setSignature('AmZ9dfhpUQ.LufHrMWycdKtOeaE3AVRDO1JLHsIc7y2aE7-RQOffo.gD');
          $gateway->setTestMode(true);

          $response = $gateway->purchase(
          array(
            'cancelUrl'=>'https://178.62.89.219/project/cart/index',
            'returnUrl'=>'https://178.62.89.219/project/cart/order_success',
            'description'=>'New PayPal Purchase',
            'amount'=>$this->session->userdata('total'),
            'currency'=>'GBP'
            )
            )->send();

            $response->redirect();
          }
          else{
            redirect('cart/no_address');
          }
        }
        else{
          redirect('cart/restricted');
        }
      }
      else{
        redirect('cart');
      }
    }

    public function no_address($offset = null)
    {
      $this->load->model('cart_model');
      $this->load->library('pagination');

      $config['base_url'] = base_url(). 'main/members';
      $config['total_rows'] = $this->cart_model->allOrders();
      $config['per_page'] = 4;
      $config['full_tag_open'] = '<ul class="pagination">';
      $config['full_tag_close'] = '</ul>';
      $config['prev_link'] = '&laquo;';
      $config['prev_tag_open'] = '<li>';
      $config['prev_tag_close'] = '</li>';
      $config['next_link'] = '&raquo;';
      $config['next_tag_open'] = '<li>';
      $config['next_tag_close'] = '</li>';
      $config['cur_tag_open'] = '<li class="active"><a href="#">';
      $config['cur_tag_close'] = '</a></li>';
      $config['num_tag_open'] = '<li>';
      $config['num_tag_close'] = '</li>';
      $config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
      $this->pagination->initialize($config);
      $this->pagination->cur_page = $offset;
      $query = $this->cart_model->getOrders($config['per_page'],$offset);

      $orderData = null;

      if($query){
        $orderData =  $query;
      }
      $data = array(
        'title' => "FalseMembers",
        'email' => $this->session->userdata('email'),
        'orders' => $orderData,
        'noAdd' => "We need an address before you can order."
      );
      $this->output->set_header("HTTP/1.0 200 OK");
      $this->output->set_header("HTTP/1.1 200 OK");
      $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
      $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
      $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
      $this->output->set_header("Pragma: no-cache");
      $this->load->view('templates/header', $data);
      $this->load->view('members', $data);
      $this->load->view('templates/footer');
    }

    public function restricted()
    {
      $data['title'] = 'Restricted';

      $this->load->view('templates/header', $data);
      $this->load->view('order_login');
      $this->load->view('templates/footer');
    }

    public function login_validation()
    {
      $this->load->library('form_validation');
      $this->form_validation->set_error_delimiters('<p> class="error">', '</p>');
      $this->form_validation->set_rules('email', 'Email', 'required|trim|htmlspecialchars|xss_clean|max_length[40]|callback_validate_credentials');
      $this->form_validation->set_rules('password', 'Password', 'required|htmlspecialchars|min_length[6]|max_length[20]|md5|trim');
      $this->load->model('model_users');
      if($this->form_validation->run())
      {
        $user_id = $this->model_users->get_userID($this->input->post('email'));
        $data = array(
          'email' => $this->input->post('email'),
          'is_logged_in' => 1,
          'user_id' => $user_id
        );
        $this->session->set_userdata($data);
        redirect('cart/index');
      }
      else
      {
        $data['title'] = 'Login';

        $this->load->view('templates/header', $data);
        $this->load->view('order_login');
        $this->load->view('templates/footer');
      }
    }

    public function order_success()
    {

      $gateway = GatewayFactory::create('PayPal_Express');
      $gateway->setUsername('calbad-facilitator_api1.hotmail.co.uk');
      $gateway->setPassword('J68RB9TEW9KG79F2');
      $gateway->setSignature('AmZ9dfhpUQ.LufHrMWycdKtOeaE3AVRDO1JLHsIc7y2aE7-RQOffo.gD');
      $gateway->setTestMode(true);

      $response = $gateway->completePurchase(
      array(
        'cancelUrl'=>'https://178.62.89.219/project/cart/index',
        'returnUrl'=>'https://178.62.89.219/project/cart/order_success',
        'description'=>'New PayPal Purchase',
        'amount'=>$this->session->userdata('total'), //Change to grandtotal variable from cart
        'currency'=>'GBP'
        )
        )->send();

        $test = $response->getData();

        if($test['ACK'] == 'Success')
        {
          //Insert into database look at save_order() below
          // And store user imformation in database.
          $user_id = $this->session->userdata('user_id');
          $order = array(
            'date' 			=> date('Y-m-d'),
            'user_id' 	=> $user_id
          );

          $order_id = $this->cart_model->insert_order($order);

          if ($cart = $this->cart->contents()):
            foreach ($cart as $item):
              $order_detail = array(
                'orderid' 		=> $order_id,
                'date' 			=> date('Y-m-d'),
                'productid' 	=> $item['id'],
                'product_name' 	=> $item['name'],
                'quantity' 		=> $item['qty'],
                'subtotal' 	=> $item['subtotal'],
                'price' 		=> $item['price'],
                'user_id' 	=> $user_id
              );

              // Insert product information with order detail, store in cart also store in database.
              $this->cart_model->insert_order_detail($order_detail);
            endforeach;
          endif;
          $cart = $this->cart->contents();
          $data = array(
            'title' => "Success",
            'test' => $test,
            'total' => $this->session->userdata('total'),
            'cart' => $cart,
            'order_id' => $order_id
          );
          $this->cart->destroy();
          $this->session->unset_userdata('total');
          $this->output->set_header("HTTP/1.0 200 OK");
          $this->output->set_header("HTTP/1.1 200 OK");
          $this->output->set_header('Last-Modified: '.gmdate('D, d M Y H:i:s', time()).' GMT');
          $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
          $this->output->set_header("Cache-Control: post-check=0, pre-check=0");
          $this->output->set_header("Pragma: no-cache");
          $this->load->view('templates/header', $data);
          $this->load->view('order_success', $data);
          $this->load->view('templates/footer');
        }
      }
    }

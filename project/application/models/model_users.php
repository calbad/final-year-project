<?php
class Model_users extends CI_Model {

  public function get_password($emailORid)
  {
    $this->db->select('password');
    $this->db->where('email', $emailORid);
    $this->db->or_where('id', $emailORid);
    $query = $this->db->get('user');
    if($query->num_rows() == 1)
    {
      return $query->row()->password;
    }
    else
    {
      return "False";
    }
  }
  public function can_log_in()
  {
    $hash = $this->get_password($this->input->post('email'));
    //$hash = (string)$hash;

    if(password_verify($this->input->post('password'), $hash))
    {
      return true;
    }
    else{
      return false;
    }
  }

  public function check_pass($user_id)
  {
    $hash = $this->get_password($user_id);

    if(password_verify($this->input->post('opass'), $hash))
    {
      return true;
    }
    else{
      return false;
    }
  }
  public function get_userID($email)
  {
    $this->db->where('email',$email);
    $query = $this->db->get('user');
    foreach ($query->result() as $row)
    {
      $user_id = $row->id;
    }
    return $user_id;

  }

  public function check_email($user_id, $email)
  {
    $this->db->where('id !=', $user_id);
    $this->db->where('email', $email);
    $query = $this->db->get('user');
    if($query->num_rows() == 1)
    {
      return true;
    }
    else
    {
      return false;
    }

  }
  public function insert_address($data)
  {
    $this->db->insert('address', $data);
  }

  public function check_address($user_id)
  {
    $this->db->where('user_id', $user_id);

    $query = $this->db->get('address');

    if($query->num_rows() == 1)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public function get_address($user_id)
  {
    $this->db->where('user_id', $user_id);

    $query = $this->db->get('address');

    if($query->num_rows() == 1)
    {
      $row = $query->row_array();
        return $row;
    }
  }

  public function add_temp_user($key)
  {
    $data = array(
      'email' => $this->input->post('email'),
      'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
      'key' => $key
    );

    $query = $this->db->insert('temp_user', $data);
    if($query)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public function is_key_valid($key)
  {
    $this->db->where('key', $key);
    $query = $this->db->get('temp_user');

    if ($query->num_rows() == 1)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  public function add_user($key)
  {
    $this->db->where('key', $key);
    $temp_user = $this->db->get('temp_user');

    if ($temp_user)
    {
      $row = $temp_user->row();

      $data = array(
        'email' => $row->email,
        'password' => $row->password
      );

      $did_add_user = $this->db->insert('user', $data);

    }
    if($did_add_user)
    {
      $this->db->where('key', $key);
      $this->db->delete('temp_user');
      return $data['email'];
    }
    return false;

  }
}
?>

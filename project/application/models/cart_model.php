<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart_model extends CI_Model {

  // Get all details ehich store in "products" table in database.
  public function get_all()
  {
    $query = $this->db->get('products');
    return $query->result_array();
  }

  public function get_colour($colour)
  {
    $this->db->where('colour', $colour);
    $query = $this->db->get('products');
    return $query->result_array();
  }

  public function get_type($type)
  {
    $this->db->where('type', $type);
    $query = $this->db->get('products');
    return $query->result_array();
  }

  public function get_size($size)
  {
    $this->db->join('product_size', 'products.serial = product_size.product_id');
    $this->db->join('size', 'size.size_id = product_size.size_id');
    $this->db->where('size.size', $size);
    $query = $this->db->get('products');
    return $query->result_array();
  }

  // Insert order date with user id in "orders" table in database.
  public function insert_order($data)
  {
    $this->db->insert('orders', $data);
    $id = $this->db->insert_id();
    return (isset($id)) ? $id : FALSE;
  }

  // Insert ordered product detail in "order_detail" table in database.
  public function insert_order_detail($data)
  {
    $this->db->insert('order_detail', $data);
  }

  function getOrders($limit,$offset){
    $this->db->select("date,product_name,quantity,price,subtotal");
    $this->db->where('user_id', $this->session->userdata('user_id'));
    $query = $this->db->get('order_detail', $limit, $offset);

    // Check if pages where found
    if ($query->num_rows() > 0)
    {
        return $query->result_array();
    }
    else
    {
        return FALSE;
    }
  }

  function allOrders(){
    $this->db->where('user_id', $this->session->userdata('user_id'));
    $this->db->from('order_detail');
    return $this->db->count_all_results();
  }

}
